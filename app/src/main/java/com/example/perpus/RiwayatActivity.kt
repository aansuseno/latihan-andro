package com.example.perpus

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TableRow
import android.widget.TextView
import android.widget.TableLayout as TableLayout

class RiwayatActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_riwayat)

        tampilRiwayat()
    }

    fun tampilRiwayat() {
        val tb = TableLayout(this)
        val judul = TableRow(this)

        val kolomNo = TextView(this)
        kolomNo.text = "No. "
        kolomNo.setTextColor(Color.BLACK)
        kolomNo.setTextSize(18F)
        judul.addView(kolomNo)
        val kolomNama = TextView(this)
        kolomNama.text = " Pembeli "
        kolomNama.setTextColor(Color.BLACK)
        kolomNama.setTextSize(18F)
        judul.addView(kolomNama)
        val kolomBarang = TextView(this)
        kolomBarang.text = " Barang "
        kolomBarang.setTextColor(Color.BLACK)
        kolomBarang.setTextSize(18F)
        judul.addView(kolomBarang)
        val kolomBayar = TextView(this)
        kolomBayar.setTextColor(Color.BLACK)
        kolomBayar.setTextSize(18F)
        kolomBayar.text = " Bayar "
        judul.addView(kolomBayar)

        tb.addView(judul)

        for (i in 0 until riwayat.size) {
            val tr = TableRow(this)

            val no = TextView(this)
            no.text = (i+1).toString()
            no.setTextColor(Color.BLACK)
            no.setTextSize(14F)
            tr.addView(no)

            val nama = TextView(this)
            nama.text = riwayat.get(i)[0]
            nama.setTextColor(Color.BLACK)
            nama.setTextSize(14F)
            tr.addView(nama)

            val barang = TextView(this)
            barang.text = riwayat.get(i)[1]
            barang.setTextColor(Color.BLACK)
            barang.setTextSize(14F)
            tr.addView(barang)

            val bayar = TextView(this)
            bayar.text = riwayat.get(i)[5]
            bayar.setTextColor(Color.BLACK)
            bayar.setTextSize(14F)
            tr.addView(bayar)

            val hapus = Button(this)
            hapus.text = "hapus"
            hapus.tag = i.toString()
            hapus.setBackgroundColor(Color.parseColor("#ff0000"))
            hapus.setOnClickListener(View.OnClickListener {
                riwayat.removeAt(i)
                tampilRiwayat()
            })
            tr.addView(hapus)

            tb.addView(tr)
        }

        findViewById<LinearLayout>(R.id.bgNya).addView(tb)
    }
}