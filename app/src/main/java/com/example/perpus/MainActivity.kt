package com.example.perpus

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.animation.AnimationUtils
import android.widget.ImageView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val animKeatas = AnimationUtils.loadAnimation(this, R.anim.keatas)
        val logo = findViewById<ImageView>(R.id.lks)
        logo.animation = animKeatas

        val animKekanan = AnimationUtils.loadAnimation(this, R.anim.kekanan)
        val logo1 = findViewById<ImageView>(R.id.pwr)
        logo1.animation = animKekanan

        Handler(Looper.getMainLooper()).postDelayed({
            intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }, 2000)
    }
}