package com.example.perpus

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText

class TransaksiActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaksi)
    }

    fun proses(view: android.view.View) {
        var intent = Intent(this, HasilActivity::class.java)
        intent.putExtra("nama", findViewById<EditText>(R.id.nama).text.toString())
        intent.putExtra("barang", findViewById<EditText>(R.id.barang).text.toString())
        intent.putExtra("harga", findViewById<EditText>(R.id.harga).text.toString())
        intent.putExtra("jml", findViewById<EditText>(R.id.jumlah).text.toString())
        startActivity(intent)
    }
}