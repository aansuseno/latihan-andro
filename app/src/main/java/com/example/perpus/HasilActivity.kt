package com.example.perpus

import android.icu.text.NumberFormat
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.Currency.*
import kotlin.collections.ArrayList

var riwayat = ArrayList<Array<String>>()

class HasilActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hasil)

        var harga = 0.0

        var nama = intent.getStringExtra("nama")
        var barang = intent.getStringExtra("barang")
        harga = intent.getStringExtra("harga").toString().toDouble()
        var jumlah = intent.getStringExtra("jml")?.toString()?.toDouble()
        var bayar = 0.0
        var diskon = 0.0
        bayar = harga* jumlah!!
        if (jumlah >= 10) {
            diskon = bayar * 0.02
            bayar = bayar - diskon
        }

        val kalender = Calendar.getInstance()
        val waktu = SimpleDateFormat("HH:mm:ss".toString()).format(kalender.time).toString()
        val tgl = SimpleDateFormat("yyyy-MM-dd".toString()).format(kalender.time).toString()

        val local = Locale("id", "ID")
        val formatter = NumberFormat.getCurrencyInstance(local)

        val totalBayar = formatter.format(bayar)
        val uangharga = formatter.format(harga)
        val uangdiskon = formatter.format(diskon)

        riwayat.add(arrayOf<String>("$nama", "$barang", "$uangharga", "$jumlah", "$uangdiskon", "$totalBayar", "$waktu", "$tgl"))

        findViewById<TextView>(R.id.namaHasil).text =": $nama"
        findViewById<TextView>(R.id.barangHasil).text = ": $barang"
        findViewById<TextView>(R.id.hargaHasil).text = ": $uangharga"
        findViewById<TextView>(R.id.jumlahHasil).text = ": ${jumlah.toInt()}"
        findViewById<TextView>(R.id.diskonHasil).text = ": $uangdiskon"
        findViewById<TextView>(R.id.totalHasil).text = ": $totalBayar"
    }
}