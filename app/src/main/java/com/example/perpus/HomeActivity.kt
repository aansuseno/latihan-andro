package com.example.perpus

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }

    fun keItem(view: android.view.View) {
        var intent = Intent(this, ItemActivity::class.java)
        startActivity(intent)
    }

    fun keTransaksi(view: android.view.View) {
        startActivity(Intent(this, TransaksiActivity::class.java))
    }

    fun keRiwayat(view: android.view.View) {
        startActivity(Intent(this, RiwayatActivity::class.java))
    }

    fun keInfo(view: android.view.View) {}
}